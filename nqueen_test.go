package minicsp

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Queen(n int) CSPModel {
	ranges := Range(0, n)

	columns := mapF(ranges, func(i int) IValue {
		return IntValue(i)
	})
	vars := mapF(ranges, func(i int) Variable {
		return Variable{
			Name:   VariableName(fmt.Sprintf("Q-%d", i)),
			Domain: columns,
			Index:  i,
		}
	})
	notSameColumns := MakeAllDiffConstraint(vars)
	fmt.Println(notSameColumns)
	notSameDiagonal := func(var1Ref, var2Ref VariableRef) Constraint {
		return Constraint{
			Name:              fmt.Sprintf("q(%d) q(%d)", var1Ref, var2Ref),
			PositionalVarRefs: []VariableRef{var1Ref, var2Ref},
			Func: func(variables []VarAssignment) bool {
				dx := int(var1Ref) - int(var2Ref)
				dy := int(variables[0].Value.(IntValue)) - int(variables[1].Value.(IntValue))
				return dx != dy && dx != -dy
			},
		}
	}

	notSameDiagonalConstraints := []Constraint{}
	for i, var_i := range vars[:len(vars)-1] {
		notSameDiagonalConstraints = append(notSameDiagonalConstraints, mapF(vars[i+1:], func(var_j Variable) Constraint {
			return notSameDiagonal(VariableRef(var_i.Index), VariableRef(var_j.Index))
		})...)
	}
	constraints := []Constraint{}
	constraints = append(constraints, notSameColumns...)
	constraints = append(constraints, notSameDiagonalConstraints...)
	return NewCSPModel(vars, constraints)
}

func Test8Queen(t *testing.T) {
	cspModel := Queen(8)
	cspModel = AC3(cspModel)
	assert.True(t, cspModel.Validate())
	cspPlan := MakePlan(cspModel)
	plannedSteps := cspPlan.GetPlannedSteps()
	vars := cspModel.GetVariables()
	for a := range plannedSteps.Generator() {
		varDef := vars[a.VarToAssign.VarIndex()]
		fmt.Println(varDef.Name, a.StepNo, varDef.Domain, len(varDef.Domain))
	}
	csp := CSPRuntimeContext{
		Model:        cspModel,
		CSPPlan:      cspPlan,
		AssignedVars: []VarAssignment{},
	}

	solutions := BacktrackAll(csp)
	assert.Equal(t, 92, len(solutions))
}

func Test24Queen(t *testing.T) {
	cspModel := Queen(24)
	cspModel = AC3(cspModel)
	assert.True(t, cspModel.Validate())
	cspPlan := MakePlan(cspModel)
	plannedSteps := cspPlan.GetPlannedSteps()
	vars := cspModel.GetVariables()
	for a := range plannedSteps.Generator() {
		varDef := vars[a.VarToAssign.VarIndex()]
		fmt.Println(varDef.Name, a.StepNo, varDef.Domain, len(varDef.Domain))
	}
	csp := CSPRuntimeContext{
		Model:        cspModel,
		CSPPlan:      cspPlan,
		AssignedVars: []VarAssignment{},
	}

	solution, success := Backtrack(csp)
	fmt.Println(solution)
	assert.True(t, success)
}

func Test24QueenLocalSearch(t *testing.T) {
	cspModel := Queen(8)
	cspModel = AC3(cspModel)
	assert.True(t, cspModel.Validate())
	assignment := cspModel.HillClimbing(2048)
	cost := cspModel.EvaluateCost(assignment)
	fmt.Println("cost", cost)
	assert.Equal(t, 0, cost)
}
