package minicsp

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValue(t *testing.T) {
	a := IntValue(1)
	b := IntValue(2)
	c := IntValue(1)
	assert.True(t, a.Equal(c))
	assert.False(t, a.Equal(b))
}

func TestCSPNotEqual(t *testing.T) {

	variableDefs := []Variable{
		{Name: "A", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 0},
		{Name: "B", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 1},
		{Name: "C", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 2},
	}

	constraints := []Constraint{
		{
			PositionalVarRefs: []VariableRef{VariableRef(variableDefs[0].Index), VariableRef(variableDefs[1].Index)},
			Func: func(variables []VarAssignment) bool {
				return !(variables[0].Value).Equal(variables[1].Value)
			},
		},

		{
			PositionalVarRefs: []VariableRef{VariableRef(variableDefs[1].Index), VariableRef(variableDefs[2].Index)},
			Func: func(variables []VarAssignment) bool {
				return !(variables[0].Value).Equal(variables[1].Value)
			},
		},
	}
	cspModel := NewCSPModel(variableDefs, constraints)
	assert.True(t, cspModel.Validate())

	cspModel = AC3(cspModel)
	cspPlan := MakePlan(cspModel)

	//csp := CSP{Variables: []*Variable{&variables[0], &variables[1], &variables[2]}, Constraints: constraints}
	csp := CSPRuntimeContext{
		Model:        cspModel,
		CSPPlan:      cspPlan,
		AssignedVars: []VarAssignment{},
	}
	/*
		AC3(&csp)
	*/
	solutions := BacktrackAll(csp)
	assert.Equal(t, 12, len(solutions))
	for _, s := range solutions {

		assert.True(t, !(s.AssignedVarBySteps[0].Value).Equal(s.AssignedVarBySteps[1].Value),
			fmt.Sprintf("%v != %v", s.AssignedVarBySteps[0].Value, s.AssignedVarBySteps[1].Value))
		assert.True(t, !(s.AssignedVarBySteps[1].Value).Equal(s.AssignedVarBySteps[2].Value),
			fmt.Sprintf("%v != %v", s.AssignedVarBySteps[1].Value, s.AssignedVarBySteps[2].Value))
	}
}

func TestCSPLarge(t *testing.T) {

	variableDefs := []Variable{
		{Name: "A", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 0},
		{Name: "B", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 1},
		{Name: "C", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 2},
	}
	// A>B>C
	constraints := []Constraint{
		{
			Description:       "A>B",
			PositionalVarRefs: []VariableRef{VariableRef(variableDefs[0].Index), VariableRef(variableDefs[1].Index)},
			Func: func(variables []VarAssignment) bool {
				return variables[0].Value.LargeThan(variables[1].Value)
			},
		},

		{
			Description:       "B>C",
			PositionalVarRefs: []VariableRef{VariableRef(variableDefs[1].Index), VariableRef(variableDefs[2].Index)},
			Func: func(variables []VarAssignment) bool {
				return variables[0].Value.LargeThan(variables[1].Value)
			},
		},
	}
	cspModel := NewCSPModel(variableDefs, constraints)
	assert.True(t, cspModel.Validate())
	cspModel = AC3(cspModel)
	assert.Equal(t, 2, len(cspModel.GetBinaryConstraints()))
	for idx, varDef := range cspModel.GetVariables() {
		fmt.Println(idx, varDef.Domain)
	}
	cspPlan := MakePlan(cspModel)

	csp := CSPRuntimeContext{
		Model:        cspModel,
		CSPPlan:      cspPlan,
		AssignedVars: []VarAssignment{},
	}

	/*
		AC3(&csp)
	*/
	solutions := BacktrackAll(csp)
	assert.Equal(t, 1, len(solutions))
	for _, s := range solutions {
		assert.True(t, !(s.AssignedVarBySteps[0].Value).Equal(s.AssignedVarBySteps[1].Value),
			fmt.Sprintf("%v != %v", s.AssignedVarBySteps[0].Value, s.AssignedVarBySteps[1].Value))
		assert.True(t, !(s.AssignedVarBySteps[1].Value).Equal(s.AssignedVarBySteps[2].Value),
			fmt.Sprintf("%v != %v", s.AssignedVarBySteps[1].Value, s.AssignedVarBySteps[2].Value))
	}
}

func TestCSPLargeNoAC(t *testing.T) {

	variableDefs := []Variable{
		{Name: "A", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 0},
		{Name: "B", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 1},
		{Name: "C", Domain: []IValue{IntValue(1), IntValue(2), IntValue(3)}, Index: 2},
	}
	// A>B>C
	constraints := []Constraint{
		{
			Description:       "A>B",
			PositionalVarRefs: []VariableRef{VariableRef(variableDefs[0].Index), VariableRef(variableDefs[1].Index)},
			Func: func(variables []VarAssignment) bool {
				return variables[0].Value.LargeThan(variables[1].Value)
			},
		},

		{
			Description:       "B>C",
			PositionalVarRefs: []VariableRef{VariableRef(variableDefs[1].Index), VariableRef(variableDefs[2].Index)},
			Func: func(variables []VarAssignment) bool {
				return variables[0].Value.LargeThan(variables[1].Value)
			},
		},
	}
	cspModel := NewCSPModel(variableDefs, constraints)
	assert.True(t, cspModel.Validate())
	for idx, varDef := range cspModel.GetVariables() {
		fmt.Println(idx, varDef.Domain)
	}
	cspPlan := MakePlan(cspModel)

	csp := CSPRuntimeContext{
		Model:        cspModel,
		CSPPlan:      cspPlan,
		AssignedVars: []VarAssignment{},
	}

	solutions := BacktrackAll(csp)
	assert.Equal(t, 1, len(solutions))
	for _, s := range solutions {
		assert.True(t, !(s.AssignedVarBySteps[0].Value).Equal(s.AssignedVarBySteps[1].Value),
			fmt.Sprintf("%v != %v", s.AssignedVarBySteps[0].Value, s.AssignedVarBySteps[1].Value))
		assert.True(t, !(s.AssignedVarBySteps[1].Value).Equal(s.AssignedVarBySteps[2].Value),
			fmt.Sprintf("%v != %v", s.AssignedVarBySteps[1].Value, s.AssignedVarBySteps[2].Value))
	}
}
