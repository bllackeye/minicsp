package minicsp

import (
	"fmt"
)

type IntValue int

func (iv IntValue) Equal(other IValue) bool {
	return iv == other
}
func (iv IntValue) LargeThan(other IValue) bool {
	otherInt := other.(IntValue)
	return int(iv) > int(otherInt)
}

func MakeAllDiffConstraint(vars []Variable) []Constraint {
	constraints := []Constraint{}
	for i, var_i := range vars[:len(vars)-1] {
		constraints = append(constraints, mapF(vars[i+1:], func(var_j Variable) Constraint {
			return Constraint{
				Name:              fmt.Sprintf("var%d=/=var%d", var_i.Index, var_j.Index),
				PositionalVarRefs: []VariableRef{VariableRef(var_i.Index), VariableRef(var_j.Index)},
				Func: func(variables []VarAssignment) bool {
					return variables[0].Value != variables[1].Value
				},
			}
		})...)
	}
	return constraints
}

func Equals(var1 Variable, var2 Variable) Constraint {
	return Constraint{
		Name:              fmt.Sprintf("var%d=/=var%d", var1.Index, var2.Index),
		PositionalVarRefs: []VariableRef{VariableRef(var1.Index), VariableRef(var2.Index)},
		Func: func(variables []VarAssignment) bool {
			return variables[0].Value == variables[1].Value
		},
	}
}

func Range(from int, to int) []int {
	ret := make([]int, to-from)
	for i := from; i < to; i++ {
		ret[i-from] = i
	}
	return ret
}
