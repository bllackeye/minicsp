package minicsp

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
Five men of different nationalities and with different jobs live in consecutive houses on a street.
These houses are painted different colors. The men have different pets and have different favorite drinks.
The following rules are provided:

1. The English man lives in a red house
2. The Spaniard owns a dog
3. The Japanese man is a painter
4. The Italian drinks tea
5. The Norwegian lives in the first house on the left
6. The green house immediately to the right of the white one
7. The photographer breeds snails
8. The diplomat lives in the yellow house
9. Milk is drunk in the middle house
10. The owner of the green house drinks coffee
11. The Norwegian’s house is next to the blue one
12. The violinist drinks orange juice
13. The fox is in a house that is next to that of the physician
14. The horse is in a house next to that of the diplomat

Who owns a zebra? And whose favorite drink is mineral water?
*/
func TestZebra(t *testing.T) {

	// initialize variables
	vars := make([]Variable, 0)
	// represent 5 men
	fiveMenDomain := []IValue{IntValue(0), IntValue(1), IntValue(2), IntValue(3), IntValue(4)}

	constraints := make([]Constraint, 0)
	Yellow := Variable{
		Domain: fiveMenDomain,
		Index:  0,
		Name:   "Yellow",
	}
	Blue := Variable{
		Domain: fiveMenDomain,
		Index:  1,
		Name:   "Blue",
	}

	Red := Variable{
		Domain: fiveMenDomain,
		Index:  2,
		Name:   "Red",
	}
	Ivory := Variable{
		Domain: fiveMenDomain,
		Index:  3,
		Name:   "Ivory",
	}
	Green := Variable{
		Domain: fiveMenDomain,
		Index:  4,
		Name:   "Green",
	}
	Norwegian := Variable{
		Domain: fiveMenDomain,
		Index:  5,
		Name:   "Norwegian",
	}
	Ukrainian := Variable{
		Domain: fiveMenDomain,
		Index:  6,
		Name:   "Ukrainian",
	}
	Englishman := Variable{
		Domain: fiveMenDomain,
		Index:  7,
		Name:   "Englishman",
	}
	Spaniard := Variable{
		Domain: fiveMenDomain,
		Index:  8,
		Name:   "Spaniard",
	}
	Japanese := Variable{
		Domain: fiveMenDomain,
		Index:  9,
		Name:   "Japanese",
	}
	Water := Variable{
		Domain: fiveMenDomain,
		Index:  10,
		Name:   "Water",
	}
	Tea := Variable{
		Domain: fiveMenDomain,
		Index:  11,
		Name:   "Tea",
	}
	Milk := Variable{
		Domain: fiveMenDomain,
		Index:  12,
		Name:   "Milk",
	}
	OrangeJuice := Variable{
		Domain: fiveMenDomain,
		Index:  13,
		Name:   "OrangeJuice",
	}
	Coffee := Variable{
		Domain: fiveMenDomain,
		Index:  14,
		Name:   "Coffee",
	}
	Kools := Variable{
		Domain: fiveMenDomain,
		Index:  15,
		Name:   "Kools",
	}
	Chesterfield := Variable{
		Domain: fiveMenDomain,
		Index:  16,
		Name:   "Chesterfield",
	}
	OldGold := Variable{
		Domain: fiveMenDomain,
		Index:  17,
		Name:   "OldGold",
	}
	LuckyStrike := Variable{
		Domain: fiveMenDomain,
		Index:  18,
		Name:   "LuckyStrike",
	}
	Parliament := Variable{
		Domain: fiveMenDomain,
		Index:  19,
		Name:   "Parliament",
	}
	Fox := Variable{
		Domain: fiveMenDomain,
		Index:  20,
		Name:   "Kools",
	}
	Horse := Variable{
		Domain: fiveMenDomain,
		Index:  21,
		Name:   "Horse",
	}
	Snails := Variable{
		Domain: fiveMenDomain,
		Index:  22,
		Name:   "Snails",
	}
	Dog := Variable{
		Domain: fiveMenDomain,
		Index:  23,
		Name:   "Dog",
	}
	Zebra := Variable{
		Domain: fiveMenDomain,
		Index:  24,
		Name:   "Zebra",
	}

	houseColors := []Variable{Yellow, Blue, Red, Ivory, Green}
	nationality := []Variable{Norwegian, Ukrainian, Englishman, Spaniard, Japanese}
	drink := []Variable{Water, Tea, Milk, OrangeJuice, Coffee}
	smoke := []Variable{Kools, Chesterfield, OldGold, LuckyStrike, Parliament}
	pet := []Variable{Fox, Horse, Snails, Dog, Zebra}
	categories := [][]Variable{houseColors, nationality, drink, smoke, pet}

	// add uniqueness constraints for each category
	for _, categoryVars := range categories {
		vars = append(vars, categoryVars...)
		constraints = append(constraints, MakeAllDiffConstraint(categoryVars)...)
	}
	// intRelConstraint checks if two int variables satisfy a binary relation
	intRelConstraint := func(var1 Variable, var2 Variable, rel func(int, int) bool) Constraint {
		return Constraint{
			PositionalVarRefs: []VariableRef{VariableRef(var1.Index), VariableRef(var2.Index)},
			Func: func(variables []VarAssignment) bool {
				v1 := int(variables[0].Value.(IntValue))
				v2 := int(variables[1].Value.(IntValue))
				return rel(v1, v2)
			}}
	}
	// nextToConstraint checks if two int vars differ by at most one
	nextToConstraint := func(var1 Variable, var2 Variable) Constraint {
		return intRelConstraint(var1, var2, func(v1, v2 int) bool { return v2 == v1+1 || v2 == v1-1 })
	}
	// offsetConstraint checks if int var1 plus offset equals var2
	offsetConstraint := func(var1 Variable, var2 Variable, offset int) Constraint {
		return intRelConstraint(var1, var2, func(v1, v2 int) bool { return v2 == v1+offset })
	}

	varAssignment := []VarAssignment{
		{VariableRef(Milk.Index), IntValue(2)},
		{VariableRef(Norwegian.Index), IntValue(0)},
	}
	constraints = append(constraints,
		Equals(Englishman, Red),
		Equals(Spaniard, Dog),
		Equals(Coffee, Green),
		Equals(Ukrainian, Tea),
		offsetConstraint(Ivory, Green, 1),
		Equals(OldGold, Snails),
		Equals(Kools, Yellow),
		nextToConstraint(Chesterfield, Fox),
		nextToConstraint(Kools, Horse),
		nextToConstraint(Norwegian, Blue),
		Equals(LuckyStrike, OrangeJuice),
		Equals(Japanese, Parliament))

	cspModel := NewCSPModel(vars, constraints)
	assert.True(t, cspModel.Validate())

	cspModel2 := ShrinkCSPModel(cspModel, varAssignment)
	assert.True(t, cspModel2.Validate())
	for _, a := range varAssignment {
		varDef := vars[a.VariableRef]
		assert.Equal(t, 1, len(varDef.Domain))
		assert.Equal(t, a.Value, varDef.Domain[0], varDef.Name)
	}
	assert.True(t, cspModel2.Validate())

	cspModel3 := AC3(cspModel2)
	assert.True(t, cspModel2.Validate())
	cspPlan := MakePlan(cspModel3)
	plannedSteps := cspPlan.GetPlannedSteps()
	for a := range plannedSteps.Generator() {
		varDef := vars[a.VarToAssign.VarIndex()]
		fmt.Println(varDef.Name, a.StepNo, varDef.Domain, len(varDef.Domain))
	}
	csp := CSPRuntimeContext{
		Model:        cspModel,
		CSPPlan:      cspPlan,
		AssignedVars: []VarAssignment{},
	}

	solution, success := Backtrack(csp)
	fmt.Println(solution)
	assert.True(t, success)
	assignments := solution.GetVarAssignmentByVarRef()
	fmt.Println(assignments)
	values := func(v Variable) int {
		return int(assignments[v.Index].Value.(IntValue))
	}
	assert.Equal(t, values(Yellow), 0)
	assert.Equal(t, values(Blue), 1)
	assert.Equal(t, values(Red), 2)
	assert.Equal(t, values(Ivory), 3)
	assert.Equal(t, values(Green), 4)
	assert.Equal(t, values(Norwegian), 0)
	assert.Equal(t, values(Ukrainian), 1)
	assert.Equal(t, values(Englishman), 2)
	assert.Equal(t, values(Spaniard), 3)
	assert.Equal(t, values(Japanese), 4)
	assert.Equal(t, values(Water), 0)
	assert.Equal(t, values(Tea), 1)
	assert.Equal(t, values(Milk), 2)
	assert.Equal(t, values(OrangeJuice), 3)
	assert.Equal(t, values(Coffee), 4)
	assert.Equal(t, values(Kools), 0)
	assert.Equal(t, values(Chesterfield), 1)
	assert.Equal(t, values(OldGold), 2)
	assert.Equal(t, values(LuckyStrike), 3)
	assert.Equal(t, values(Parliament), 4)
	assert.Equal(t, values(Fox), 0)
	assert.Equal(t, values(Horse), 1)
	assert.Equal(t, values(Snails), 2)
	assert.Equal(t, values(Dog), 3)
	assert.Equal(t, values(Zebra), 4)
}
