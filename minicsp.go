package minicsp

import (
	"math/rand"
	"sort"
	"time"

	"golang.org/x/exp/constraints"
)

type Iterable[T any] struct {
	data []T
}

func NewIterable[T any](data []T) *Iterable[T] {
	return &Iterable[T]{data: data}
}

func (it *Iterable[T]) Generator() <-chan T {
	ch := make(chan T)

	go func() {
		for _, value := range it.data {
			ch <- value
		}
		close(ch)
	}()

	return ch
}

type VariableName string

// filter will accept a slice and a filter function to return a subset of the elements that evaluates true
func filter[T any](arr []T, filterFn func(T) bool) []T {
	filtered := make([]T, len(arr))
	idx := 0
	for _, a := range arr {
		if filterFn(a) {
			filtered[idx] = a
			idx += 1
		}
	}

	return filtered[0:idx]
}

// exists returns true if there is one element satisfies the condition
func exists[T any](arr []T, conditionFn func(T) bool) bool {
	for _, a := range arr {
		if conditionFn(a) {
			return true
		}
	}
	return false
}

// contains will accept a slice and an element and will return true if the element is contained in the slice
func contains(elems []IValue, v IValue) bool {
	for _, s := range elems {
		if v.Equal(s) {
			return true
		}
	}

	return false
}

// mapF will accept a slice and function and apply the function on each
// element of the slice and return the resulting slice
func mapF[T any, M any](a []T, f func(T) M) []M {
	n := make([]M, len(a))
	for i, e := range a {
		n[i] = f(e)
	}
	return n
}

func mapWithIndex[T any, M any](a []T, f func(pos int, t T) M) []M {
	n := make([]M, len(a))
	for i, e := range a {
		n[i] = f(i, e)
	}
	return n
}

func reduce[T any, R any](slice []T, reducer func(R, T) R, initialValue R) R {
	result := initialValue
	for _, value := range slice {
		result = reducer(result, value)
	}
	return result
}

func reduceMap[K comparable, T any, R any](m map[K]T, reducer func(R, K, T) R, initialValue R) R {
	result := initialValue
	for key, value := range m {
		result = reducer(result, key, value)
	}
	return result
}

type IValue interface {
	Equal(val IValue) bool
	LargeThan(val IValue) bool
}

type Variable struct {
	Name   VariableName
	Index  int
	Domain []IValue
}

type VariableRef int

type CSPVarContext interface {
	GetVariable(i int) Variable
}

func (ref VariableRef) VarDef(ctx CSPVarContext) Variable {
	return ctx.GetVariable(int(ref))
}

func (ref VariableRef) VarIndex() int {
	return int(ref)
}

type Constraint struct {
	Name              string
	Description       string
	PositionalVarRefs []VariableRef
	Func              func(variables []VarAssignment) bool
}

type VarAssignment struct {
	VariableRef
	Value IValue
}

type CSPModel interface {
	Validate() bool
	GetVariables() []Variable
	GetConstraints() []Constraint
	GetVariable(i int) Variable
	GetBinaryConstraints() []Constraint
	GetUnaryConstraints() []Constraint
	Size() int
	EvaluateCost(assignment []VarAssignment) int
	HillClimbing(maxIteration int) []VarAssignment
}

func NewCSPModel(vars []Variable, constraints []Constraint) CSPModel {
	return CSPModelImpl{
		variableDefs:   vars,
		constraintDefs: constraints,
	}
}

type CSPModelImpl struct {
	variableDefs   []Variable
	constraintDefs []Constraint
}

func (m CSPModelImpl) Validate() bool {
	for i, varDef := range m.variableDefs {
		if i != varDef.Index {
			return false
		}
	}
	return true
}

func (m CSPModelImpl) GetVariable(index int) Variable {
	return m.variableDefs[index]
}

func (m CSPModelImpl) Size() int {
	return len(m.variableDefs)
}

func (m CSPModelImpl) GetBinaryConstraints() []Constraint {
	return filter(m.constraintDefs, func(c Constraint) bool {
		return len(c.PositionalVarRefs) == 2
	})

}

func (m CSPModelImpl) GetUnaryConstraints() []Constraint {
	return filter(m.constraintDefs, func(c Constraint) bool {
		return len(c.PositionalVarRefs) == 1
	})

}

func (m CSPModelImpl) GetConstraints() []Constraint {
	return m.constraintDefs
}

func (m CSPModelImpl) GetVariables() []Variable {
	return m.variableDefs
}

func (m CSPModelImpl) HillClimbing(maxSteps int) []VarAssignment {
	rand.Seed(time.Now().UnixNano())

	// Random initial assignment
	current := make([]VarAssignment, m.Size())
	for _, v := range m.GetVariables() {
		current[v.Index] = VarAssignment{
			VariableRef: VariableRef(v.Index),
			Value:       v.Domain[rand.Intn(len(v.Domain))],
		}
	}

	currentCost := m.EvaluateCost(current)

	for step := 0; step < maxSteps; step++ {
		neighbor := getNeighbor(current, m)
		neighborCost := m.EvaluateCost(neighbor)

		// If the neighboring solution is better (i.e., has a lower cost), move to it
		if neighborCost < currentCost {
			current = neighbor
			currentCost = neighborCost
		}

		// Exit early if a perfect solution is found
		if currentCost == 0 {
			break
		}
	}
	return current
}

func getNeighbor(assignment []VarAssignment, csp CSPModel) []VarAssignment {
	neighbor := make([]VarAssignment, csp.Size())
	for k, v := range assignment {
		neighbor[k] = v
	}

	// Randomly select a variable and change its assignment
	varToChange := csp.GetVariable(rand.Intn(csp.Size()))
	possibleValues := varToChange.Domain
	neighbor[varToChange.Index] = VarAssignment{
		VariableRef: VariableRef(varToChange.Index),
		Value:       possibleValues[rand.Intn(len(possibleValues))],
	}

	return neighbor
}

func (m CSPModelImpl) EvaluateCost(assignment []VarAssignment) int {
	return reduce(m.GetConstraints(), func(result int, constraint Constraint) int {
		constraintVarAssignment := mapF(constraint.PositionalVarRefs, func(varRef VariableRef) VarAssignment {
			return assignment[varRef]
		})
		return result + ifElse(constraint.Func(constraintVarAssignment), 0, 1)
	}, 0)
}

type CSPPlan interface {
	GetPlannedStep(stepNo int) PlannedStep
	GetPlannedSteps() *Iterable[PlannedStep]
	Size() int
}

type CSPPlanImpl struct {
	plannedSteps []PlannedStep
}

func (p CSPPlanImpl) GetPlannedStep(stepNo int) PlannedStep {
	return p.plannedSteps[stepNo]
}

func (p CSPPlanImpl) Size() int {
	return len(p.plannedSteps)
}

func (p CSPPlanImpl) GetPlannedSteps() *Iterable[PlannedStep] {
	return NewIterable(p.plannedSteps)
}

type PlannedStep struct {
	StepNo             StepNo
	VarToAssign        VariableRef
	MaturedConstraints []ConstraintEvaluation
	FCConstraints      []ConstraintEvaluation
}

type ConstraintEvaluation struct {
	StepByConstraintVarPos []int // hold the position of variable in assignment. subscript i is the i-th variableDef in the constraint.
	Def                    Constraint
	MaturedAtStep          StepNo
	FCAtStep               StepNo
}

type CSPAssignment struct {
	AssignedVarBySteps []VarAssignment
}

func (a CSPAssignment) GetVarAssignmentByVarRef() []VarAssignment {
	assignments := make([]VarAssignment, len(a.AssignedVarBySteps))
	for _, v := range a.AssignedVarBySteps {
		assignments[v.VarIndex()] = v
	}
	return assignments
}

type CSPRuntimeContext struct {
	Model        CSPModel
	CSPPlan      CSPPlan
	AssignedVars []VarAssignment
}

func minRemainingValue(var1, var2 Variable) bool {
	return len(var1.Domain) < len(var2.Domain)
}

func IsConstraintMatured(constraintDef Constraint, assignedVarDefs []Variable) bool {
	for _, constraintVarDef := range constraintDef.PositionalVarRefs {
		if !exists(assignedVarDefs, func(v Variable) bool {
			return v.Index == constraintVarDef.VarIndex()
		}) {
			return false
		}
	}
	return true
}

func MakeVarAssignmentPlan(varByIndices []Variable) PlannedStepByVars {
	index := make([]int, len(varByIndices))
	for iStep, v := range varByIndices {
		index[v.Index] = iStep
	}
	return index
}

type StepNo int

type PlannedStepByVars []int

type Plan interface {
	GetPlannedStep(varRef VariableRef) StepNo
}

func (p PlannedStepByVars) GetPlannedStep(varRef VariableRef) StepNo {
	return StepNo(p[varRef.VarIndex()])
}

func Top2[T constraints.Ordered](l []T) (T, T) {
	if len(l) == 2 {
		if l[0] > l[1] {
			return l[0], l[1]
		}
		return l[1], l[0]
	}
	top1, top2 := Top2(l[1:])
	if l[0] > top1 {
		return l[0], top1
	} else if top1 > l[0] && l[0] > top2 {
		return top1, l[0]
	}
	return top1, top2
}

func makeConstraintEvaluationPlan(c Constraint, variablePlan Plan, maturedAtStep StepNo, fcAtStep StepNo) ConstraintEvaluation {
	return ConstraintEvaluation{
		Def: c,
		StepByConstraintVarPos: mapF(c.PositionalVarRefs, func(varRef VariableRef) int {
			return int(variablePlan.GetPlannedStep(varRef)) //varDefIndex[varRef.VarIndex()]
		}),
		MaturedAtStep: maturedAtStep,
		FCAtStep:      fcAtStep,
	}
}

func sortByStepDistance(fcConstraintEvaluations []ConstraintEvaluation) []ConstraintEvaluation {
	sort.Slice(fcConstraintEvaluations, func(i, j int) bool {
		fc1 := fcConstraintEvaluations[i]
		fc2 := fcConstraintEvaluations[j]
		return fc1.MaturedAtStep-fc1.FCAtStep > fc2.MaturedAtStep-fc2.FCAtStep
	})
	return fcConstraintEvaluations
}

func MakePlan(model CSPModel) CSPPlan {
	stepToVarDef := mapF(model.GetVariables(), func(v Variable) Variable {
		return v
	})
	sort.Slice(stepToVarDef, func(i, j int) bool {
		return minRemainingValue(stepToVarDef[i], stepToVarDef[j])
	})
	varAssignmentPlan := MakeVarAssignmentPlan(stepToVarDef)
	plannedConstraints := mapF(model.GetConstraints(), func(c Constraint) ConstraintEvaluation {
		steps := mapF(c.PositionalVarRefs, func(varRef VariableRef) StepNo {
			return varAssignmentPlan.GetPlannedStep(varRef)
		})
		if len(steps) == 1 {
			return makeConstraintEvaluationPlan(c, varAssignmentPlan, steps[0], -1)
		}
		maturedAtStep, fcAtStep := Top2(steps)
		return makeConstraintEvaluationPlan(c, varAssignmentPlan, maturedAtStep, fcAtStep)
	})
	plannedSteps := mapWithIndex(stepToVarDef, func(iStep int, def Variable) PlannedStep {
		return PlannedStep{
			StepNo:      StepNo(iStep),
			VarToAssign: VariableRef(def.Index),
			MaturedConstraints: filter(plannedConstraints, func(o ConstraintEvaluation) bool {
				return int(o.MaturedAtStep) == iStep
			}),
			FCConstraints: sortByStepDistance(filter(plannedConstraints, func(o ConstraintEvaluation) bool {
				return int(o.FCAtStep) == iStep && len(o.StepByConstraintVarPos) > 2 // we don't do forward check for ac consistent binary constraints as it is useless.
			})),
		}
	},
	)
	return CSPPlanImpl{
		plannedSteps: plannedSteps,
	}
}

type ConstraintDefs []Constraint

func AC3(cspModel CSPModel) CSPModel {
	unaryConstraintGroupByVariable := reduce(cspModel.GetUnaryConstraints(), func(groups map[int]ConstraintDefs, c Constraint) map[int]ConstraintDefs {
		if constraintDefs, ok := groups[c.PositionalVarRefs[0].VarIndex()]; ok {
			groups[c.PositionalVarRefs[0].VarIndex()] = append(constraintDefs, c)
			return groups
		} else {
			groups[c.PositionalVarRefs[0].VarIndex()] = []Constraint{c}
			return groups
		}
	}, map[int]ConstraintDefs{})

	varDefUpdates := reduceMap(unaryConstraintGroupByVariable, func(result map[int]Variable, key int, unaryConstraintDefs ConstraintDefs) map[int]Variable {
		varDef, reduced := reduceDomain(cspModel, VariableRef(key), unaryConstraintDefs)
		if reduced {
			result[varDef.Index] = varDef
			return result
		}
		return result
	}, map[int]Variable{})

	nodeConsistentModel := CSPModelImpl{
		variableDefs: mapF(cspModel.GetVariables(), func(v Variable) Variable {
			if varDef, ok := varDefUpdates[v.Index]; ok {
				return varDef
			}
			return v
		}),
		constraintDefs: cspModel.GetConstraints(),
	}

	return ensureAC(nodeConsistentModel)
}

func updateCSPModel(cspModel CSPModel, varDefUpdate Variable) CSPModel {
	return CSPModelImpl{
		constraintDefs: cspModel.GetConstraints(),
		variableDefs: mapF(cspModel.GetVariables(), func(v Variable) Variable {
			if v.Index == varDefUpdate.Index {
				return varDefUpdate
			}
			return v
		}),
	}
}

func ShrinkCSPModel(cspModel CSPModel, varAssignments []VarAssignment) CSPModel {
	return CSPModelImpl{
		constraintDefs: cspModel.GetConstraints(),
		variableDefs: reduce(varAssignments, func(result []Variable, a VarAssignment) []Variable {
			oldVarDef := result[a.VariableRef.VarIndex()]
			result[oldVarDef.Index] = Variable{
				Name:   oldVarDef.Name,
				Index:  oldVarDef.Index,
				Domain: []IValue{a.Value},
			}
			return result
		}, cspModel.GetVariables()),
	}
}

func ensureAC(cspModel CSPModel) CSPModel {
	return reduce(mapF(cspModel.GetVariables(), func(v Variable) int {
		return v.Index
	}), func(result CSPModel, varIndex int) CSPModel {
		return ensureACUponEvent(result, VariableRef(varIndex), result.GetBinaryConstraints())
	}, cspModel)
}

// ensureACUponEvent ensures an variable is arc-consistent
// varUpdateEvent the variable to exam,
// binaryConstraintDefs -- all the binaryConstrains of the csp problem.
// returns a new csp model with arc-consistent.
// 3 things are done here, traverse the graph, filter the node, and summarize the result, should separate them.
func ensureACUponEvent(cspModel CSPModel, varUpdateEvent VariableRef, binaryConstraintDefs []Constraint) CSPModel {
	for _, binaryConstraintDef := range binaryConstraintDefs {
		if varUpdateEvent == binaryConstraintDef.PositionalVarRefs[0] {
			revisedVarDef, revised := reviseSecond(cspModel, binaryConstraintDef)
			if revised {
				return ensureACUponEvent(updateCSPModel(cspModel, revisedVarDef), VariableRef(revisedVarDef.Index), binaryConstraintDefs)
			}
		} else if varUpdateEvent == binaryConstraintDef.PositionalVarRefs[1] {
			revisedVarDef, revised := reviseFirst(cspModel, binaryConstraintDef)
			if revised {
				return ensureACUponEvent(updateCSPModel(cspModel, revisedVarDef), VariableRef(revisedVarDef.Index), binaryConstraintDefs)
			}
		}
	}
	return cspModel
}

// applicable to Variable that present in a constraints with only one variable
func reduceDomain(ctx CSPVarContext, varRef VariableRef, unaryConstraints []Constraint) (Variable, bool) {
	varDef := varRef.VarDef(ctx)
	values := filter(varDef.Domain, func(val IValue) bool {
		varAssignments := []VarAssignment{
			{
				VariableRef: varRef,
				Value:       val,
			},
		}
		for _, c := range unaryConstraints {
			if !c.Func(varAssignments) {
				return false
			}
		}
		return true
	})
	if len(values) < len(varDef.Domain) {
		return Variable{
			Name:   varDef.Name,
			Index:  varDef.Index,
			Domain: values,
		}, true
	}
	return varDef, false
}

func ifElse[T any](condition bool, If T, Else T) T {
	if condition {
		return If
	}
	return Else
}

// applicable to variables that present in a binary constraint (constraints with only two variables)
func reviseFirst(ctx CSPVarContext, constraint Constraint) (Variable, bool) {
	variable1 := constraint.PositionalVarRefs[0].VarDef(ctx)
	variable2 := constraint.PositionalVarRefs[1].VarDef(ctx)
	values := filter(variable1.Domain, func(var1Value IValue) bool {
		var1Assignment := VarAssignment{
			VariableRef: constraint.PositionalVarRefs[0],
			Value:       var1Value,
		}
		for _, var2Value := range variable2.Domain {
			var2Assignment := VarAssignment{
				VariableRef: constraint.PositionalVarRefs[1],
				Value:       var2Value,
			}
			if constraint.Func([]VarAssignment{var1Assignment, var2Assignment}) {
				return true
			}
		}
		return false
	})
	if len(values) < len(constraint.PositionalVarRefs[0].VarDef(ctx).Domain) {
		return Variable{
			Name:   variable1.Name,
			Domain: values,
			Index:  variable1.Index,
		}, true
	}
	return variable1, false
}

func reviseSecond(ctx CSPVarContext, constraint Constraint) (Variable, bool) {
	variable1 := constraint.PositionalVarRefs[0].VarDef(ctx)
	variable2 := constraint.PositionalVarRefs[1].VarDef(ctx)

	values := filter(variable2.Domain, func(var2Value IValue) bool {
		var2Assignment := VarAssignment{
			VariableRef: constraint.PositionalVarRefs[1],
			Value:       var2Value,
		}
		for _, var1Value := range variable1.Domain {
			var1Assignment := VarAssignment{
				VariableRef: constraint.PositionalVarRefs[0],
				Value:       var1Value,
			}
			if constraint.Func([]VarAssignment{var1Assignment, var2Assignment}) {
				return true
			}
		}
		return false
	})
	if len(values) < len(variable2.Domain) {
		return Variable{
			Name:   variable2.Name,
			Domain: values,
			Index:  variable2.Index,
		}, true
	}
	return variable2, false
}

func isComplete(csp CSPRuntimeContext) bool {
	return len(csp.AssignedVars) == csp.CSPPlan.Size()
}

func isConsistent(csp CSPRuntimeContext, plannedVariable PlannedStep, value IValue) bool {
	tentativeAssignment := append(csp.AssignedVars, VarAssignment{
		VariableRef: plannedVariable.VarToAssign,
		Value:       value,
	})
	for _, maturedConstraintEvaluation := range plannedVariable.MaturedConstraints {
		constraintDef := maturedConstraintEvaluation.Def
		variableRefs := maturedConstraintEvaluation.StepByConstraintVarPos
		assignment := mapF(variableRefs, func(stepNo int) VarAssignment {
			return tentativeAssignment[stepNo]
		})
		if !constraintDef.Func(assignment) {
			return false
		}
	}
	for _, fcConstraintEvaluation := range plannedVariable.FCConstraints {
		fcStepNo := int(fcConstraintEvaluation.MaturedAtStep)
		fcVar := csp.CSPPlan.GetPlannedStep(fcStepNo).VarToAssign.VarDef(csp.Model)
		isFcFailed := !exists(fcVar.Domain, func(val IValue) bool {
			fcAssignment := mapF(fcConstraintEvaluation.StepByConstraintVarPos, func(stepNo int) VarAssignment {
				if stepNo == fcStepNo {
					return VarAssignment{
						VariableRef: VariableRef(fcVar.Index),
						Value:       val,
					}
				}
				return tentativeAssignment[stepNo]
			})
			return fcConstraintEvaluation.Def.Func(fcAssignment)
		})
		if isFcFailed {
			return false
		}
	}
	return true
}

func Backtrack(csp CSPRuntimeContext) (CSPAssignment, bool) {
	if isComplete(csp) {
		return CSPAssignment{
			csp.AssignedVars,
		}, true
	}
	stepNo := len(csp.AssignedVars)
	plannedVariable := csp.CSPPlan.GetPlannedStep(stepNo)
	domain := plannedVariable.VarToAssign.VarDef(csp.Model).Domain
	for i := range domain {
		tentativeValue := domain[i]
		if isConsistent(csp, plannedVariable, tentativeValue) {
			currentCsp := CSPRuntimeContext{
				Model:   csp.Model,
				CSPPlan: csp.CSPPlan,
				AssignedVars: append(csp.AssignedVars, VarAssignment{
					VariableRef: plannedVariable.VarToAssign,
					Value:       tentativeValue,
				}),
			}
			if nextCsp, consistent := Backtrack(currentCsp); consistent {
				return CSPAssignment{
					nextCsp.AssignedVarBySteps,
				}, true
			}
		}
	}
	return CSPAssignment{
		csp.AssignedVars,
	}, false
}

func BacktrackAll(csp CSPRuntimeContext) []CSPAssignment {
	if isComplete(csp) {
		return []CSPAssignment{
			{
				csp.AssignedVars,
			},
		}
	}
	stepNo := len(csp.AssignedVars)
	plannedVariable := csp.CSPPlan.GetPlannedStep(stepNo)
	domain := plannedVariable.VarToAssign.VarDef(csp.Model).Domain
	allCspAssignments := mapF(domain, func(tentativeValue IValue) []CSPAssignment {
		if isConsistent(csp, plannedVariable, tentativeValue) {
			currentCsp := CSPRuntimeContext{
				Model:   csp.Model,
				CSPPlan: csp.CSPPlan,
				AssignedVars: append(csp.AssignedVars, VarAssignment{
					VariableRef: plannedVariable.VarToAssign,
					Value:       tentativeValue,
				}),
			}
			return BacktrackAll(currentCsp)
		}
		return []CSPAssignment{}
	})
	return reduce(allCspAssignments, func(result, current []CSPAssignment) []CSPAssignment {
		if len(current) > 0 {
			return append(result, current...)
		}
		return result
	}, []CSPAssignment{})
}
