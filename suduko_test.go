package minicsp

import (
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Sudoku implementation of a Sudoku puzzle solver. The particular puzzle being
// solved here has been taken straight from the wikipedia page.
// Puzzle: https://en.wikipedia.org/wiki/Sudoku#/media/File:Sudoku_Puzzle_by_L2G-20050714_standardized_layout.svg
// Solution: https://en.wikipedia.org/wiki/Sudoku#/media/File:Sudoku_Puzzle_by_L2G-20050714_solution_standardized_layout.svg
// In this solution, cells are labeled by the 3x3 sector (out of the 9 large boxes) as a letter in the range A-I,
// and an integer in the range 1-9 indicating the cell's position in the sector.
// For reference, here is an example grid:
// [A1 A2 A3 B1 B2 B3 C1 C2 C3]
// [A4 A5 A6 B4 B5 B6 C4 C5 C6]
// [A7 A8 A9 B7 B8 B9 C7 C8 C9]
// [D1 D2 D3 E1 E2 E3 F1 F2 F3]
// [D4 D5 D6 E4 E5 E6 F4 F5 F6]
// [D7 D8 D9 E7 E8 E9 F7 F8 F9]
// [G1 G2 G3 H1 H2 H3 I1 I2 I3]
// [G4 G5 G6 H4 H5 H6 I4 I5 I6]
// [G7 G8 G9 H7 H8 H9 I7 I8 I9]
// This solution enforces Arc consistency on all binary constraints in the problem,
// resulting in a very fast solve
const SIZE = 9

var blockLabels = [SIZE]string{"A", "B", "C", "D", "E", "F", "G", "H", "I"}
var numbers = [SIZE]int{1, 2, 3, 4, 5, 6, 7, 8, 9}
var ranges = [SIZE]int{0, 1, 2, 3, 4, 5, 6, 7, 8}

type sudokuVariableDefs []Variable

func resolveVarDef(v Variable) (string, int) {
	return blockLabels[v.Index/SIZE], numbers[v.Index%SIZE]
}

func getCellRowCol(v Variable) (int, int) {
	return v.Index / 9, v.Index % 9
}

func getVarIndex(blockRow, blockCol int, idx int) VariableRef {
	zeroBasedIdx := idx - 1
	cellRow := 3*blockRow + zeroBasedIdx/3
	cellCol := 3*blockCol + zeroBasedIdx%3
	return VariableRef(cellRow*9 + cellCol)
}

/*
A B C
D E F
G H I
*/
type blockFn func(i int) VariableRef

func A(i int) VariableRef {
	return getVarIndex(0, 0, i)
}
func B(i int) VariableRef {
	return getVarIndex(0, 1, i)
}
func C(i int) VariableRef {
	return getVarIndex(0, 2, i)
}
func D(i int) VariableRef {
	return getVarIndex(1, 0, i)
}
func E(i int) VariableRef {
	return getVarIndex(1, 1, i)
}
func F(i int) VariableRef {
	return getVarIndex(1, 2, i)
}
func G(i int) VariableRef {
	return getVarIndex(2, 0, i)
}
func H(i int) VariableRef {
	return getVarIndex(2, 1, i)
}
func I(i int) VariableRef {
	return getVarIndex(2, 2, i)
}

func getBlockRowColLabel(v Variable) (int, int, string) {
	cellRow, cellCol := getCellRowCol(v)
	blockRow := cellRow / 3
	blockCol := cellCol / 3
	return blockRow, blockCol, blockLabels[blockRow*3+blockCol]
}

func TestSudoku(t *testing.T) {
	tenDomain := []IValue{IntValue(1), IntValue(2), IntValue(3), IntValue(4), IntValue(5), IntValue(6), IntValue(7), IntValue(8), IntValue(9)}
	vars := []Variable{}
	constraints := []Constraint{}

	for i, letter := range blockLabels {
		for j, number := range numbers {
			varName := VariableName(letter + strconv.Itoa(number))
			varDef := Variable{
				Name:   varName,
				Index:  i*9 + j,
				Domain: tenDomain,
			}
			vars = append(vars, varDef)
		}
	}

	// for each block, add uniqueness constraint within block
	for _, letter := range blockLabels {
		letterVars := filter(vars, func(v Variable) bool {
			_, _, blockLetter := getBlockRowColLabel(v)
			return blockLetter == letter
		})
		constraints = append(constraints, MakeAllDiffConstraint(letterVars)...)
	}

	for _, row := range ranges {
		rowVars := filter(vars, func(v Variable) bool {
			cellRow, _ := getCellRowCol(v)
			return row == cellRow
		})
		constraints = append(constraints, MakeAllDiffConstraint(rowVars)...)
	}

	for _, col := range ranges {
		colVars := filter(vars, func(v Variable) bool {
			_, cellCol := getCellRowCol(v)
			return col == cellCol
		})
		constraints = append(constraints, MakeAllDiffConstraint(colVars)...)
	}
	cspModel := NewCSPModel(vars, constraints)
	assert.True(t, cspModel.Validate())
	assert.Equal(t, A(2), VariableRef(1), "A2")
	assert.Equal(t, A(4), VariableRef(9), "A4")
	varAssignment := []VarAssignment{
		{A(1), IntValue(5)},
		{A(2), IntValue(3)},
		{A(4), IntValue(6)},
		{A(8), IntValue(9)},
		{A(9), IntValue(8)},
		{B(2), IntValue(7)},
		{B(4), IntValue(1)},
		{B(5), IntValue(9)},
		{B(6), IntValue(5)},
		{C(8), IntValue(6)},
		{D(1), IntValue(8)},
		{D(4), IntValue(4)},
		{D(7), IntValue(7)},
		{E(2), IntValue(6)},
		{E(4), IntValue(8)},
		{E(6), IntValue(3)},
		{E(8), IntValue(2)},
		{F(3), IntValue(3)},
		{F(6), IntValue(1)},
		{F(9), IntValue(6)},
		{G(2), IntValue(6)},
		{H(4), IntValue(4)},
		{H(5), IntValue(1)},
		{H(6), IntValue(9)},
		{H(8), IntValue(8)},
		{I(1), IntValue(2)},
		{I(2), IntValue(8)},
		{I(6), IntValue(5)},
		{I(8), IntValue(7)},
		{I(9), IntValue(9)},
	}
	cspModel2 := ShrinkCSPModel(cspModel, varAssignment)
	assert.True(t, cspModel2.Validate())
	//vars := cspModel2.GetVariables()
	for _, a := range varAssignment {
		varDef := vars[a.VariableRef]
		assert.Equal(t, 1, len(varDef.Domain))
		assert.Equal(t, a.Value, varDef.Domain[0], varDef.Name)
	}
	assert.True(t, cspModel2.Validate())

	cspModel3 := AC3(cspModel2)
	assert.True(t, cspModel2.Validate())
	cspPlan := MakePlan(cspModel3)
	plannedSteps := cspPlan.GetPlannedSteps()
	for a := range plannedSteps.Generator() {
		varDef := vars[a.VarToAssign.VarIndex()]
		fmt.Println(varDef.Name, a.StepNo, varDef.Domain, len(varDef.Domain))
	}
	csp := CSPRuntimeContext{
		Model:        cspModel,
		CSPPlan:      cspPlan,
		AssignedVars: []VarAssignment{},
	}

	solution, success := Backtrack(csp)
	fmt.Println(solution)
	assert.True(t, success)
	assignments := solution.GetVarAssignmentByVarRef()
	rowLetterSets := [3][3]blockFn{{A, B, C}, {D, E, F}, {G, H, I}}
	rowNumberSets := [3][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}

	for _, letterSet := range rowLetterSets {
		for _, numberSet := range rowNumberSets {
			sum := 0
			for _, letter := range letterSet {
				for _, number := range numberSet {
					varRef := letter(number)
					sum += int(assignments[varRef].Value.(IntValue))
				}
			}
			assert.Equal(t, 45, sum)
		}
	}

	columnLetterSets := [3][3]blockFn{{A, D, G}, {B, E, H}, {C, F, I}}
	columnNumberSets := [3][3]int{{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}

	for _, letterSet := range columnLetterSets {
		for _, numberSet := range columnNumberSets {
			sum := 0
			for _, letter := range letterSet {
				for _, number := range numberSet {
					varRef := letter(number)
					sum += int(assignments[varRef].Value.(IntValue))
				}
			}
			assert.Equal(t, 45, sum)
		}
	}

	letters := [9]blockFn{A, B, C, D, E, F, G, H, I}
	for _, letter := range letters {
		sum := 0
		for num := 1; num <= 9; num++ {
			varRef := letter(num)
			sum += int(assignments[varRef].Value.(IntValue))
		}
		assert.Equal(t, 45, sum)
	}
}
